document.addEventListener("DOMContentLoaded", function() {


});
jQuery( document ).ready(function($) {

	$( '.nav__item' ).on('click',function() {
  	  if ( !$( this ).parent().is( '.nav__item-current') ) {
  	  	$( '.nav__item' ).removeClass('nav__item-current');
  	  	$( this ).addClass('nav__item-current');
  	  } 
    });

    $( '.menu-mobile' ).on('click',function() {
      	$('.header-top__menu').toggle();
    });

	$('.h-slider').slick({
		autoplay: true, 
		slidesToScroll: 1,
		dots: true,
		prevArrow: '<svg class="h-slider__prev"><use xlink:href="/img/sprite.svg#left-arrow"></use></svg>',
		nextArrow: '<svg class="h-slider__next"><use xlink:href="/img/sprite.svg#right-arrow"></use></svg>'
	});

	$('.b-products__slider').slick({
		//autoplay: true, 
		centerMode: true,
		slidesToScroll: 1,
		slidesToShow: 5,
		prevArrow: '<span class="arrow-right"></span>',
		nextArrow: '<sapn class="arrow-left"></span>',
		responsive: [
  			{
		    breakpoint: 991,
			    settings: {
			        slidesToShow: 2,
			        infinite: true,
			    }
		    },
		    {
		    breakpoint: 768,
			    settings: {
			        slidesToShow: 1,
			    }
		    }
		  ]
	});
	
	$('.foto-slider').slick({
		slidesToScroll: 1,
		slidesToShow: 1,
		prevArrow: '<span class="arrow-right"></span>',
		nextArrow: '<sapn class="arrow-left"></span>',
	});
	
	$('.video-slider').slick({
		slidesToScroll: 1,
		slidesToShow: 1,
		prevArrow: '<span class="arrow-right"></span>',
		nextArrow: '<sapn class="arrow-left"></span>',
	});

	$('.b-news__slider').slick({
		slidesToScroll: 1,
		slidesToShow: 2,
		prevArrow: '<span class="arrow-right"></span>',
		nextArrow: '<sapn class="arrow-left"></span>',
		responsive: [
  			{
		    breakpoint: 991,
			    settings: {
			        slidesToShow: 1,
			    }
		    }		   
		  ]
	});

	$('.b-testimon__slider').slick({
		slidesToScroll: 1,
		slidesToShow: 3,
		prevArrow: '<span class="arrow-right"></span>',
		nextArrow: '<sapn class="arrow-left"></span>',
		responsive: [
  			{
		    breakpoint: 991,
			    settings: {
			        slidesToShow: 2,
			    }
		    },
		    {
		    breakpoint: 768,
			    settings: {
			        slidesToShow: 1,
			    }
		    }
		  ]
	});
	

	$('.b-retractable__title, .b-gallery__header').html(function(){
		let text= $(this).text().trim().split(" ");
		let first = text.shift();
		return (text.length > 0 ? "<span class='accent'>"+ first + "</span> " : first) + text.join(" ");
	});

});